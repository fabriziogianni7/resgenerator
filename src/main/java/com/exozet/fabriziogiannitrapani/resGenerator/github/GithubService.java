package com.exozet.fabriziogiannitrapani.resGenerator.github;

import java.util.Arrays;
import java.util.List;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.exozet.fabriziogiannitrapani.resGenerator.events.Items;
import com.exozet.fabriziogiannitrapani.resGenerator.events.Repo;
import com.exozet.fabriziogiannitrapani.resGenerator.events.User;

@Component
public class GithubService {
	private final RestTemplate restTemplate;


	public  GithubService(RestTemplateBuilder builder) {
		this.restTemplate = builder.build();
	}
	
	//returns the 1st element of Items array which is by default the one with the highest score
	public Items fetchUser(String username) {
		String query = "https://api.github.com/search/users?q="+username;
		return this.restTemplate.getForEntity(query, User.class).getBody().getItems()[0];
	}
	
	
	public Repo[] fetchRepos (String username){
		Items user = fetchUser(username);
		String reposUrl= user.getReposUrl();
		
		return this.restTemplate.getForEntity(reposUrl, Repo[].class).getBody();
//		return Arrays.asList(fetchUser(username).getReposUrl()());
		
//		return List<Repo> repo = this.restTemplate.getForEntity(reposUrl, Repo.class);
	}
}
