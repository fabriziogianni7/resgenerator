package com.exozet.fabriziogiannitrapani.resGenerator.events;


import java.time.OffsetDateTime;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


public class Repo {
	private final String repoName;
	private final String repoUrl;
	private final String description;
	private final OffsetDateTime creationDate;
	private final String language;
	private final Long watchers;
	
	@JsonCreator
	public Repo(@JsonProperty("name") String repoName,
			@JsonProperty("html_url") String repoUrl,
			@JsonProperty("description") String description, 
			@JsonProperty("created_at") OffsetDateTime creationDate, 
			@JsonProperty("language") String language,
			@JsonProperty("watchers") Long watchers) {
		super();
		this.repoName = repoName;
		this.repoUrl = repoUrl;
		this.description = description;
		this.creationDate = creationDate;
		this.language = language;
		this.watchers = watchers;
	}
	
	
	public String getRepoName() {
		return repoName;
	}
	public String getRepoUrl() {
		return repoUrl;
	}
	public String getDescription() {
		return description;
	}
	public OffsetDateTime getCreationDate() {
		return creationDate;
	}
	public String getLanguage() {
		return language;
	}
	public Long getWatchers() {
		return watchers;
	}

}
