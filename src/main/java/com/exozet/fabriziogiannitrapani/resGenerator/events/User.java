package com.exozet.fabriziogiannitrapani.resGenerator.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class User {
	private  Long totCount;
	private  Boolean incompleteRes;
	private  Items[] items;
	
	
	@JsonCreator
	public User(@JsonProperty("total_count") Long totCount,
			@JsonProperty("incomplete_results") Boolean incompleteRes,
			@JsonProperty("items") Items[] items) {
		super();
		this.totCount = totCount;
		this.incompleteRes = incompleteRes;
		this.items= items;
	}
	public long getTotCount() {
		return totCount;
	}
	public Boolean getIncompleteRes() {
		return incompleteRes;
	}
	public Items[] getItems() {
		return items;
	}


	


}
