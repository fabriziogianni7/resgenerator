package com.exozet.fabriziogiannitrapani.resGenerator.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


public class Items {
	private final String login;
	private final Long id;
	private final String nodeId;
	private final String avatarUrl;
	private final String gravatarUrl;
	private final String url;
	private final String htmlUrl;
	private final String followersUrl;
	private final String followingUrl;
	private final String gistsUrl;
	private final String starredUrl;
	private final String subscriptionsUrl;
	private final String organizationsUrl;
	private final String reposUrl;
	private final String eventsUrl;
	private final String recEventUrl;
	private final String type;
	private final Boolean siteAdmin;
	private final Long score;
	
	@JsonCreator
	public Items(@JsonProperty("login") String login,
			@JsonProperty("id") Long id, 
			@JsonProperty("node_id") String nodeId,
			@JsonProperty("avatar_url") String avatarUrl,
			@JsonProperty("gravatar_id") String gravatarUrl,
			@JsonProperty("url") String url,
			@JsonProperty("html_url") String htmlUrl,
			@JsonProperty("followers_url") String followersUrl, 
			@JsonProperty("following_url") String followingUrl, 
			@JsonProperty("gists_url") String gistsUrl, 
			@JsonProperty("starred_url") String starredUrl, 
			@JsonProperty("subscriptions_url") String subscriptionsUrl,
			@JsonProperty("organizations_url") String organizationsUrl,
			@JsonProperty("repos_url") String reposUrl, 
			@JsonProperty("events_url") String eventsUrl,
			@JsonProperty("received_events_url") String recEventUrl,
			@JsonProperty("type") String type,
			@JsonProperty("site_admin") Boolean siteAdmin,
			@JsonProperty("score") Long score
			) {
		super();
		this.login = login;
		this.id = id;
		this.nodeId = nodeId;
		this.avatarUrl = avatarUrl;
		this.gravatarUrl = gravatarUrl;
		this.url = url;
		this.htmlUrl = htmlUrl;
		this.followersUrl = followersUrl;
		this.followingUrl = followingUrl;
		this.gistsUrl = gistsUrl;
		this.starredUrl = starredUrl;
		this.subscriptionsUrl = subscriptionsUrl;
		this.organizationsUrl = organizationsUrl;
		this.reposUrl = reposUrl;
		this.eventsUrl = eventsUrl;
		this.recEventUrl = recEventUrl;
		this.type = type;
		this.siteAdmin = siteAdmin;
		this.score = score;
	}
	
	public String getLogin() {
		return login;
	}
	public Long getId() {
		return id;
	}
	public String getNodeId() {
		return nodeId;
	}
	public String getAvatarUrl() {
		return avatarUrl;
	}
	public String getGravatarUrl() {
		return gravatarUrl;
	}
	public String getUrl() {
		return url;
	}
	public String getHtmlUrl() {
		return htmlUrl;
	}
	public String getFollowersUrl() {
		return followersUrl;
	}
	public String getFollowingUrl() {
		return followingUrl;
	}
	public String getGistsUrl() {
		return gistsUrl;
	}
	public String getStarredUrl() {
		return starredUrl;
	}
	public String getSubscriptionsUrl() {
		return subscriptionsUrl;
	}
	public String getOrganizationsUrl() {
		return organizationsUrl;
	}
	public String getReposUrl() {
		return reposUrl;
	}
	public String getEventsUrl() {
		return eventsUrl;
	}
	public String getRecEventUrl() {
		return recEventUrl;
	}
	public String getType() {
		return type;
	}
	public Boolean getSiteAdmin() {
		return siteAdmin;
	}
	public Long getScore() {
		return score;
	}
	

}
