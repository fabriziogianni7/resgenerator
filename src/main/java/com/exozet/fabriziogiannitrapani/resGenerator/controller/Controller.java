package com.exozet.fabriziogiannitrapani.resGenerator.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.exozet.fabriziogiannitrapani.resGenerator.events.Items;
import com.exozet.fabriziogiannitrapani.resGenerator.events.Repo;
import com.exozet.fabriziogiannitrapani.resGenerator.github.GithubService;
//commit
@RestController
public class Controller {

	private final GithubService githubService;
	
	public Controller (GithubService githubService) {
		this.githubService = githubService;
	}

	@GetMapping("/")
	public String index(Model model ) {
	
		return "index";
	}

	
	@GetMapping("/user")
	@ResponseBody
	public Items fetchUsers(@RequestParam String login) {
		Items response = this.githubService.fetchUser(login);
		return response;
	}
	
	@GetMapping("/user/repo")
	@ResponseBody
	public String fetchRepos(Model model,@RequestParam String login) {
		List<Repo> repos = Arrays.asList(this.githubService.fetchRepos(login));
		
		model.addAttribute("repos", repos);
		
		//TODO raccogliere tutti i linguaggi  dividerli in gruppi e mostrare percentuale
//		Map<Integer, String> langList = new HashMap<Integer,String>();
//		for(Repo r : repos) {
//			langList.entrySet().add();
//		}
		
		return "resume";
	}
}
