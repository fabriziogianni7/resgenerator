package com.exozet.fabriziogiannitrapani.resGenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResGeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ResGeneratorApplication.class, args);
	}

}
